#include <iostream>


//Поле 3x3 и стандартные правила Х 0


using namespace std;

int WinLose(char a[3][3]) {

    int str = 0;
    for (int i=0; i < 3; i++) {
        if (a[i][0] == a[i][1] && a[i][1] == a[i][2] && (a[i][0] == 'x' || a[i][0] == '0')) {
            str=1;
        }
    }

    int stolb = 0;
    for (int j=0; j < 3; j++) {
        if (a[0][j] == a[1][j] && a[1][j] == a[2][j] && (a[0][j] == 'x' || a[0][j] == '0')) {
            stolb=1;
        }
    }

    int MainDiagonal=0;
    if (a[0][0] == a[1][1] && a[1][1] == a[2][2] && (a[0][0] == 'x' || a[0][0] == '0')) {
        MainDiagonal=1;
    }

    int SecondDiagonal=0;
    if (a[0][2] == a[1][1] && a[1][1] == a[2][0] && (a[0][2] == 'x' || a[0][2] == '0')) {
        SecondDiagonal=1;
    }

    if (str == 1 || stolb == 1 || MainDiagonal == 1 || SecondDiagonal == 1 ) {
            return 1;
        }
        else {
            return 0;
        }
}

int FreeSpace(char a[3][3]) {
    int space=1;
    for (int i=0; i < 3; i++) {
        for (int j=0; j < 3; j++) {
            if (a[i][j] != 'x' && a[i][j] != '0') {
                space=0;
                break;
            }
        }
    }
    return space;
}

int main()
{
    cout<<endl;
    cout<<endl;
    cout<<"           0    1     2"<<endl;
    cout<<"         ________________"<<endl;
    cout<<"      0  |    |    |    |"<<endl;
    cout<<"         |____|____|____|"<<endl;
    cout<<"      1  |    |    |    |"<<endl;
    cout<<"         |____|____|____|"<<endl;
    cout<<"      2  |    |    |    |"<<endl;
    cout<<"         |____|____|____|"<<endl;

    int Symbol=1;
    char a[3][3]={};
    int x,y;
    while(1){
    if (Symbol==1) {
        cout<<"Coordinati dlya krest"<<endl;
        cin>>x>>y;
        a[x][y]='x';
        Symbol=0;
    }
    else {
        cout<<"Coordinati dlya nulikov"<<endl;
        cin>>x>>y;
        a[x][y]='0';
        Symbol=1;
    }

    cout<<endl;
    cout<<endl;
    cout<<"           0    1     2"<<endl;
    cout<<"         ________________"<<endl;
    cout<<"      0  |"<<a[0][0]<<"   |"<<a[0][1]<<"   |"<<a[0][2]<<"   |"<<endl;
    cout<<"         |____|____|____|"<<endl;
    cout<<"      1  |"<<a[1][0]<<"   |"<<a[1][1]<<"   |"<<a[1][2]<<"   |"<<endl;
    cout<<"         |____|____|____|"<<endl;
    cout<<"      2  |"<<a[2][0]<<"   |"<<a[2][1]<<"   |"<<a[2][2]<<"   |"<<endl;
    cout<<"         |____|____|____|"<<endl;

    if ((WinLose(a) == 1) && (Symbol == 0)) {
        cout<<"Krest win"<<endl;
        break;
    }
    if ((WinLose(a)  == 1) && (Symbol == 1)) {
        cout<<"Nuli win"<<endl;
        break;
    }
    if (FreeSpace(a) == 1) {
        cout<<"No space";
        break;
    }
    }
}




